# Demo Spring Boot, RX Java y Docker

La siguiente api fue implementada para realizar cambios de moneda.  cuenta con los endpoints:
Para simplificar la logica de conversión se uso el dolar como la moneda de referencia para el cambio.  

El API usa RXJava y Spring Boot empleando Java 11 y Maven, cuenta con los siguientes endpoints:

*/token/register* encargada de generar el bearer token para la utilizacion de los demas servicios

*/api/exchange*  encargada de realizar el cambio de moneda, los nombres de los campos empleados se cambiaron a ingles.
  
*/api/exchange/currencies* devuelve todos los currencies almacenados en H2  
  
*/api/exchange/currencies/{currency}* que permite actualizar el valor de cambio  

Para poder generar la imagen es primero necesario generar *demo-0.0.1-SNAPSHOT.jar* mediante

`mvn clean package `

En caso no tenga Maven instalado en su maquina puede emplear el siguiente jar [link](https://drive.google.com/file/d/1fw-s7yyCk2rfuFaebz6EnigupGs-qwho/view?usp=sharing) 

Ha sido dockerizada en el desarrollo usando: 

`docker build --tag=test:latest . `

y su ejecucion local mediante:  

` docker container run -p 8080:8443 test`

Debe tomar en consideracion que 8443 es el puerto interno y 8080 el externo, siendo *beta* el nombre asignado al contenedor.

<img src="/screenshots/ss_local.png" alt="screenshot pruebas locales"/>  

Es necesario crear el bearer token para poder usar el api  

<img src="/screenshots/generate_token.png" alt="bearer"/>  

En caso no generes el token te dara un error 403  

<img src="/screenshots/exchange_without_token.png" alt="403 error"/>  

Una vez generado el token y empleado en el request puedes hacer uso del API  

<img src="/screenshots/exchange_with_token.png" alt="200"/>
