package com.bcp.example.demo.controller;

import com.bcp.example.demo.entity.Currency;
import com.bcp.example.demo.models.BaseWebResponse;
import com.bcp.example.demo.models.CurrencyResponse;
import com.bcp.example.demo.models.ExchangeRequest;
import com.bcp.example.demo.models.ExchangeResponse;
import com.bcp.example.demo.service.exchange.ExchangeService;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api/exchange")
public class ExchangeController {

    @Autowired
    private ExchangeService exchangeService;

    @PostMapping(
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    ) public Single<ResponseEntity<BaseWebResponse<ExchangeResponse>>> convertCurrency(
            @RequestBody ExchangeRequest exchangeRequest) {
        return exchangeService.convertCurrency(exchangeRequest).subscribeOn(Schedulers.io())
                .map(s -> ResponseEntity.ok(BaseWebResponse.successWithData(s)));
    }

    @GetMapping(
            value = "/currencies",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Single<ResponseEntity<BaseWebResponse<List<CurrencyResponse>>>> getAllCurrencies() {
        return Single.fromObservable(exchangeService.getAllCurrencies()
                .subscribeOn(Schedulers.io())
                .map(currencies -> ResponseEntity.ok(BaseWebResponse.successWithData(toCurrencyResponse(currencies)))));
    }

    @PostMapping(
            value = "/currencies/{currency}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Single<ResponseEntity<BaseWebResponse<Currency>>> updateCurrency(@PathVariable(value = "currency") String currency, @RequestParam(value = "value", defaultValue = "1") Float usdValue) {
        return exchangeService.updateCurrency(usdValue, currency)
                .subscribeOn(Schedulers.io())
                .map(currencies -> ResponseEntity.ok(BaseWebResponse.successWithData(currencies)));
    }

    private List<CurrencyResponse> toCurrencyResponse(List<Currency> currencies){
        return currencies.stream().map(currency -> CurrencyResponse.builder()
                        .name(currency.getName())
                        .toUsd(currency.getUsdValue())
                        .build())
                .collect(Collectors.toList());
    }

}
