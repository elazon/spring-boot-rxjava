package com.bcp.example.demo.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExchangeResponse {
    private Float amount;
    private Float amountConverted;
    private String fromCurrency;
    private String toCurrency;
}
