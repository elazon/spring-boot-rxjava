package com.bcp.example.demo.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExchangeRequest {
    private Float amount;
    private String fromCurrency;
    private String toCurrency;
}
