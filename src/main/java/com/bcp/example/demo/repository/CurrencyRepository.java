package com.bcp.example.demo.repository;

import com.bcp.example.demo.entity.Currency;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CurrencyRepository extends JpaRepository<Currency,String> {
    Currency findByName(String name);
    Currency save(Currency currency);
}
