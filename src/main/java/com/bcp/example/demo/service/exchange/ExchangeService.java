package com.bcp.example.demo.service.exchange;

import com.bcp.example.demo.entity.Currency;
import com.bcp.example.demo.models.ExchangeRequest;
import com.bcp.example.demo.models.ExchangeResponse;
import io.reactivex.Observable;
import io.reactivex.Single;

import java.util.List;

public interface ExchangeService  {
    Single<ExchangeResponse> convertCurrency(ExchangeRequest exchangeRequest);
    Observable<List<Currency>> getAllCurrencies();
    Single<Currency> updateCurrency(Float usdValue, String name);
}
