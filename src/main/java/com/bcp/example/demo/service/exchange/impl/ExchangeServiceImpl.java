package com.bcp.example.demo.service.exchange.impl;

import com.bcp.example.demo.entity.Currency;
import com.bcp.example.demo.entity.Transaction;
import com.bcp.example.demo.models.ExchangeRequest;
import com.bcp.example.demo.models.ExchangeResponse;
import com.bcp.example.demo.repository.CurrencyRepository;
import com.bcp.example.demo.repository.TransactionRepository;
import com.bcp.example.demo.service.exchange.ExchangeService;
import io.reactivex.Observable;
import io.reactivex.Single;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service
public class ExchangeServiceImpl implements ExchangeService {
    @Autowired
    private TransactionRepository transactionRepository;
    @Autowired
    private CurrencyRepository currencyRepository;

    @Override
    public Single<ExchangeResponse> convertCurrency(ExchangeRequest exchangeRequest){
        return saveTransaction(exchangeRequest);
    }

    @Override
    public Observable<List<Currency>> getAllCurrencies(){
        return Observable.just(currencyRepository.findAll());
    }

    @Override
    public Single<Currency> updateCurrency(Float usdValue, String name){
        return updateInH2(usdValue,name);
    }

    Single<Currency> updateInH2(Float usdValue, String name){
        return Single.create(singleSuscriber -> {
        Currency currency = currencyRepository.findByName(name);
        if(Objects.isNull(currency) ) singleSuscriber.onError(new EntityNotFoundException());
        else{
            currency.setUsdValue(usdValue);
            Currency update = currencyRepository.save(currency);
            singleSuscriber.onSuccess(update);
        }
    });
    }

    Single<ExchangeResponse> saveTransaction(ExchangeRequest exchangeRequest){
        return Single.create(singleSuscriber -> {
            Currency from = currencyRepository.findByName(exchangeRequest.getFromCurrency());
            Currency to = currencyRepository.findByName(exchangeRequest.getToCurrency());

            if(Objects.isNull(from) || Objects.isNull(to))
                singleSuscriber.onError(new EntityNotFoundException());
            else{
                Float converted = convertTo(to.getUsdValue(),from.getUsdValue(), exchangeRequest.getAmount());
                transactionRepository.save(toTransaction(exchangeRequest, converted));
                ExchangeResponse response = ExchangeResponse.builder()
                        .amount(exchangeRequest.getAmount())
                        .amountConverted(converted)
                        .fromCurrency(exchangeRequest.getFromCurrency())
                        .toCurrency(exchangeRequest.getToCurrency())
                        .build();
                singleSuscriber.onSuccess(response);
            }
        });
    }

    private Float convertTo(Float to, Float from, Float amount){
        return (from*amount)/to;
    }

    private Transaction toTransaction(ExchangeRequest er, Float converted){
        return Transaction.builder()
                .from(er.getFromCurrency())
                .to(er.getToCurrency())
                .amount(er.getAmount())
                .amountConverted(converted)
                .createdAt(Date.from(Instant.now()))
                .build();
    }

}
