DROP TABLE IF EXISTS currencies;

CREATE TABLE currencies (
                              id INT AUTO_INCREMENT  PRIMARY KEY,
                              name VARCHAR(250) NOT NULL,
                              usd_value FLOAT NOT NULL DEFAULT 0
);

INSERT INTO currencies (name, usd_value) VALUES
                                                             ('PEN', 0.24501),
                                                             ('USD', 1),
                                                             ('JPY', 0.00877),
                                                             ('GBP', 1.33906),
                                                             ('EUR', 1.14641);
